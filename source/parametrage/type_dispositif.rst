.. _type_dispositif:

############################
Saisir un type de dispositif
############################


Il est proposé de décrire dans ce paragraphe la saisie d'un type de dispositif dans le menu paramétrage.

Les types de dispositif sont listés dans l'option "type de dispositif" du menu paramétrage

.. image:: ../_static/tab_typedispositif.png

Il est possible de creer ou modifier un type de dispositif dans le formulaire ci dessous

.. image:: ../_static/form_typedispositif.png


Il est saisie :

- l'abréviation du type
- le libellé du type

L'onglet dispositif permet de visualiser les dispositifs correspondants au type de dispositif choisi.