.. _modele:

################
Saisir un modèle
################


Il est proposé de décrire dans ce paragraphe la saisie d'un modèle dans le menu paramétrage.

Les modèles sont listées dans l'option modèle du menu paramétrage

.. image:: ../_static/tab_modele.png

Il est possible de creer ou modifier un modèle dans le formulaire ci dessous

.. image:: ../_static/form_modele.png


Il est saisie :

- l'abréviation du modèle
- le libellé
- un coefficient (inutile pour le moment)

L'onglet dispositif permet de visualiser les dispositifs publicitaires correspondants au modèle selectioné.