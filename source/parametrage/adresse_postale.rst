.. _adresse_postale:

###########################
Créer des adresses postales
###########################


Il est proposé de décrire dans ce paragraphe la saisie d'une adresse postale.


Les adresses postales sont listées dans l onglet "adresse_postale"

.. image:: ../_static/tab_adresse_postale.png


Il est possible de creer ou modifier une adresse postale dans le formulaire ci dessous

.. image:: ../_static/form_adresse_postale.png



Il est saisie :

- le numero (dans la voie)

- le complement de numero (bis, ter ...)

- le libellé

Avec l'interface openLayers, il est saisi le ponctuel représentant le point d'adresse.
