.. _index:



Nous vous proposons dans ce chapitre de modifier le parametrage par défaut.

Le paramétrage de base est disponnible dans data/pgsql/parametrage.sql


.. toctree::

    categorie.rst
    modele.rst
    regle.rst
    type_dispositif.rst
    adresse_postale.rst
    reglement.rst
