.. _categorie:

####################
Saisir une catégorie
####################


Il est proposé de décrire dans ce paragraphe la saisie d'une catégorie.

Une catégorie est composé d'une abréviation et d'un libellé.

Il existe par défaut 3 catégorie :

- enseigne
- pré-enseigne
- publicité

Ces catégorie sont utilisés pour décrire un dispositif ce qui permet de lui appliquer la taxe correspondante.

Les catégories sont listées dans l'option catégorie du menu paramétrage

.. image:: ../_static/tab_categorie.png


Il est possible de creer ou modifier une catégorie dans le formulaire ci dessous

.. image:: ../_static/form_categorie.png


Les onglets site et type_dispositif listent les sites et type de dispositif se rapportant a la catégorie selectionnée
