.. _regle:

################
Saisir une règle
################


Il est proposé de décrire dans ce paragraphe la saisie d'une règle dans le menu paramétrage.

Les règles sont utilisées lors du calcul des factures.En effet en fonction de la surface totale
des dispositifs le montant des taxes varie.

Les règles sont listées dans l'option règle du menu paramétrage

.. image:: ../_static/tab_regle.png

Il est possible de creer ou modifier une règle dans le formulaire ci dessous

.. image:: ../_static/form_regle.png


Il est saisie :

- le libellé de la règle
- la catégorie de dispositif a laquelle s'applique la règle
- l'encadrement de la surface maximum et minimum pour laquelle la règle s'applique
- le tarif appliqué
- une case a cocher si le dispositif est numérique