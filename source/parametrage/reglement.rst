.. _reglement:

#########
Règlement
#########


Il est proposé de décrire dans ce paragraphe la saisie d'un règlement.


Les règlements sont listés dans l onglet "règlement"

.. image:: ../_static/tab_reglement.png


Il est possible de creer ou modifier un règlement dans le formulaire ci dessous

.. image:: ../_static/form_reglement.png



Il est saisie :

- le libellé

- le n° insee

Avec l'interface openLayers, il est saisi le polygone représentant la surface du règlement.
