.. _index:



Nous vous proposons dans ce chapitre d'utiliser openTaxePub après avoir décrit les
principes de l'application.



.. toctree::

    principes.rst
    entreprise.rst
    site.rst
    dispositif.rst