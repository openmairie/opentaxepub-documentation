.. _entreprise:

#####################
Saisir une entreprise
#####################


Il est proposé de décrire dans ce paragraphe la saisie de l'entreprise :

Les entreprises sont listées dans "application -> entreprise"

.. image:: ../_static/tab_entreprise.png

Il est possible de creer ou modifier une entreprise dans le formulaire ci dessous

.. image:: ../_static/form_entreprise.png


Il est saisie :

- la raison sociale (obligatoire)

- Un N° Siret

- Un code tiers (obligatoire pour envoyer sur sédit)

- l'adresse de l'entreprise (seul le numéro de rue et la voie sont nécéssaire a la géolocalisation -> voir paragraphe intégration/géolocalisation)

- une adresse de contact (mail,téléphone,fax)

- l'exploitant (civilité, nom, prénom)

- le statut de l'entreprise (Annonceur, Operateur, Enseigne, Préenseigne)


et les onglets d'entreprise

onglet site (voir paragraphe utilisation/site)

onglet dispositif en lecture seule(voir paragraphe utilisation/dispositif)

onglet facture (voir paragraphe traitement/facture)


La géolocalisation des entreprises, on y accède en cliquant sur le "globe terrestre" depuis la liste des entreprises

.. image:: ../_static/sig_entreprise.png