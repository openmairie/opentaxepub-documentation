.. _dispositif:

####################
Saisir un dispositif
####################


Il est proposé de décrire dans ce paragraphe la saisie d'un dispositif.

Les dispositifs sont accessibles en création/modification dans l'onglet "dispositif" de "site" :

.. image:: ../_static/form_site_dispositif.png

Il est possible de creer ou modifier un dispositif dans le formulaire ci dessous

.. image:: ../_static/form_dispositif.png


Il est saisie :

- la catégorie du dispositif (obligatoire)

- le modèle

- le type de dispositif

- une case a cocher qui définit si le dispositif est numérique (ce qui se répercutera sur la taxe)

- les dimensions horizontales et verticales en cm

- le nombre de panneau publicitaire

- la surface en m² qui est calculée automatiquement

- une photo du dispositif

- une date de début et de fin

- le nombre de mois ou le dispositif est affiché si le dispositif est temporaire