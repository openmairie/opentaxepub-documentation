.. _principes:


#############
Les principes
#############


openTaxePub a pour but de gérer les taxes sur les publicités d'une collectivité.


Les principes sont les suivants :

- les entreprises sont gérées dans la table "entreprise"

- les sites sont gérés dans la table "site"

- les dispositifs publicitaires sont gérés dans la table "dispositif"

- il peut être saisi un ou plusieurs sites pour une entreprise (lien relationnel)

- il peut être saisi un ou plusieurs dispositifs publicitaires pour un site (lien relationnel)


Les tables contenant des objets graphiques peuvent être remplacés par des vues (voir integration)
sur un sig.

Pour calculer les taxes sur la publicité il faut donc ajouter des dispositifs qui sont crée dans des sites qui appartiennent a une entreprise.

    
Nous vous proposons maintenant d'utiliser openTaxePub :

- de saisir une entreprise, un site et un dispositif

- géolocaliser : l'entreprise et le site

- associer des photos a un site et/ou a un dispositif


    
    

