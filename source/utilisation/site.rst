.. _site:

##############
Saisir un site
##############


Il est proposé de décrire dans ce paragraphe la saisie de site dans l'onglet "site" :

.. image:: ../_static/tab_site.png

Il est possible de creer ou modifier un site dans le formulaire ci dessous

.. image:: ../_static/form_site.png


Il est saisie :

- la raison sociale de l'entreprise a laquelle le site appartient (l'entreprise doit avoir été insérée précédemment). On peut choisir l'entreprise soit dans la liste déroulante, soit en rentrant le nom ou une partie du nom de l'entreprise dans le champ juste dessous puis de cliquer sur la flèche

- des observations sur le site

- l'adresse du site pour pouvoir le géolocaliser (voir paragraphe intégration/géolocalisation)

- une photo du site


et l'onglet dispositif (voir paragraphe utilisation/dispositif)


La géolocalisation des sites, on y accède en cliquant sur le "globe terrestre" depuis la liste des sites

.. image:: ../_static/sig_site.png