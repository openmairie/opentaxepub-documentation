.. _index:



Nous vous proposons dans ce chapître de décrire l'intégration d'openTaxePub dans le système d'information

- principes d'une application composite (mashup)

- integration avec des vues (adressage, emprise, filaire)

- les fonds de cartes internet

- le tableau de bord parametrable (widget)



.. toctree::

    principes_integration.rst
    geolocalisation.rst
    widget.rst
    

