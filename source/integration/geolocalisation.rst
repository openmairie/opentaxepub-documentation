.. _geolocalisation:

##################
La géolocalisation
##################


Il est décrit ici les principes d'integration dans un SIG :

- l'utilisation de vues pour se connecter sur des bases externes

- la géolocalisation des entreprises et sites



===========================================
Les vues sur des bases internes ou externes
===========================================

Il est possible avec postgresql et dblink d'utiliser une base de données externe
( voir installation et utilisation de dblink dans le guide du développeur openMairie)

Il est possible aussi de mettre openTaxePub dans un schéma et de faire une simple vue sur le
schéma du SIG

Ces vues se substitue alors aux tables de base et elles ne sont pas modifiables.

Les vues proposées sont dans init_metier_vue.sql et concerne :

- adresse_postale

- reglement

  
=============================
Le dessin de site openTaxePub
=============================

Il est possible de déssiner un point sur une carte représentant la localisation d'une entreprise ou d'un site
avec le point d'une adresse postale.