.. _redimensionner:

##############
Redimensionner
##############


Pour améliorer la rapidité de l'affichage des photos de sites ou de dispositifs ainsi que pour qu'elles soient plus visibles,
il peut être utile de les redimmensionner.
Ce traitement se propose de les redimensionner en fonction de la largeur, la hauteur étant calculée ensuite automatiquement.
Par défaut la largeur est de 600 pixels pour permettre un affichage optimal.


traitement  :

.. image:: ../_static/redimensionner.png


validation :

.. image:: ../_static/redimensionner_done.png

