.. _fillien:

##############
Lien finances
##############

Cet onglet permet d'exporter les montants des taxes publicitaires de chaque entreprises vers le logiciel de comptabilité sédit.
Ce traitement doît être éffectué après avoir calculer les factures.
Si l'entreprise n'a pas de code tiers elle sera omise.

Le résultat est un fichier texte a importer dans sédit (suivant la procédure filien).

Il pourra être constitué d'autres traitements pour d'autres logiciels de comptabilité municipal.