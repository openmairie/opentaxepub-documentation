.. _facture:

########
Factures
########


Les factures sont listés dans l'onglet Facture

.. image:: ../_static/tab_facture.png

Il sont créés automatiquement par le traitement calculer facture (voir chapitre traitement/Calculer une facture)


La table contient :

- la raison sociale de l'entreprise pour laquelle la facture a été calculé

- la surface totale de tout les dispositifs en m²

- le montant total a payer

- l'année de création de la facture

- le solde (En Cours ou Terminé)


et l'onglet titre ligne qui contient une ligne pour chaque dispositif de l'entreprise