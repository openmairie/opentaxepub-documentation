.. _editerfacture:

###############
Editer Factures
###############

Cet onglet permet de sortir toutes les factures pour l'année en cours de toutes les entreprises au sein d'un même PDF pour faire une impression globale.