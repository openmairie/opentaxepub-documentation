.. _calculfacture:

#####################
Calculer les factures
#####################


Ce traitement permet de calculer pour l'année en cours les factures pour toutes les entreprises insérées dans la base.
Si le traitement a déja été exécuté pour l'année en cours, les factures précédentes seront supprimées.

Les factures des années précédentes ne seront pas détruites.

Les factures calculées se retrouvent ensuite dans l'onglet traitement/facture (voir chapitre traitement/facture)

La formule des calculs de taxe est celle ci:

:: surface totale du dispositif x tarif de la somme totale de touts les dispositifs x nombre de mois ou le dispositif est utilisé/12


traitement  :

.. image:: ../_static/calcul_facture.png


validation :

.. image:: ../_static/calcul_facture_done.png

Attention le traitement peut durer quelques minutes.

Parametrage du calcul en dyn/var.inc ::
    
    // precision pour le calcul de surface : nombre de chiffre apres la virgule pour la surface calculee : 1 ou 2
    $surface_precision =1; 