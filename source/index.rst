.. opencimetiere documentation master file, created by
   sphinx-quickstart on Wed Jul 20 17:30:38 2011.


=============================
openTaxePub 1.0 Documentation
=============================

Créé dans le cadre de la mise en oeuvre du SIG communautaire de l'ACCM (Arles camargue crau montagnette)
openTaxePub a pour but de gérer les taxes sur la publicité
http://www.openmairie.org/catalogue/opentaxepub

Ce document a pour but de guider les développeurs et les utilisateurs
dans la prise en main du projet.

Il est proposé un premier chapitre permettant la prise en main d'openTaxePub.

Dans un deuxieme chapitre, il est proposé de décrire le paramètrage de l'application
afin de personnaliser votre application openTaxePub et l'adapter à votre organisation.

Le troisième chapitre décrit les traitements spécifiques à openTaxePub.

Enfin le quatriéme chapître est consacré aux principes d'intégration avec
d'autres applications afin de constituer des applications composites (mashup) :

- intégration dans le SIG en utilisant les  API google, API bing, API osm,

- integration par l'intermédiaire de vue depuis le SIG

- intégration avec le logiciel de comptabilité sédit


Bonne lecture et n'hésitez pas à venir discuter du projet avec la communauté à l'adresse suivante : https://communaute.openmairie.org/c/autres-applications-openmairie/opentaxepub

Cette création est mise à disposition selon le Contrat Paternité-Partage des
Conditions Initiales à l'Identique 2.0 France disponible en ligne
http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
Creative Commons, 171 Second Street, Suite 300, San Francisco,
California 94105, USA.

   
Utilisation
===========

.. toctree::
   :maxdepth: 3

   utilisation/index.rst


Parametrage
===========

.. toctree::
   :maxdepth: 3

   parametrage/index.rst

Traitement
==========

.. toctree::
   :maxdepth: 3

   traitement/index.rst
   
Integration
===========

.. toctree::
   :maxdepth: 3

   integration/index.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

* bibliographie

  http://www.openmairie.org/telechargement/openMairie-Guidedudveloppeur.pdf/view


Contributeurs
=============

* `Rémy PERU <remy.peru@free.fr>`_
* `Francois Raynaud <contact@openmairie.org>`_